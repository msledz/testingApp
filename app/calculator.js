const is = require('is_js')

exports.add = (firstNumber, secondNumber) => {
  if (is.not.number(firstNumber) || is.not.number(secondNumber)) {
    return 'please provide numbers'
  }

  return firstNumber + secondNumber
}

exports.subtract = (firstNumber, secondNumber) => {
  if (is.not.number(firstNumber) || is.not.number(secondNumber)) {
    return 'please provide numbers'
  }
  return firstNumber - secondNumber
}

exports.multiplication = (firstNumber, secondNumber) => {
  if (is.not.number(firstNumber) || is.not.number(secondNumber)) {
    return 'please provide numbers'
  }
  return firstNumber * secondNumber
}

exports.division = (firstNumber, secondNumber) => {
  if (is.not.number(firstNumber) || is.not.number(secondNumber)) {
    return 'please provide numbers'
  }
  if (secondNumber === 0) {
    return 'second number should not be 0'
  }
  return firstNumber / secondNumber
}
