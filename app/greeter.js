const is = require('is_js')

module.exports = class Greeter {
  constructor(name = 'Magda') {
    this.name = name
  }

  sayHello() {
    return `Hello ${this.name}`
  }

  setName(name) {
    if (is.not.string(name)) {
      return 'name should be string'
    }

    this.name = name
  }
}
