const expect = require('chai').expect

const calculator = require('../app/calculator')

describe('Calculator', () => {
  describe('Add', () => {
    it('should add two number variables', () => {
      const firstNumber = 10
      const secondNumber = 5
      const addResult = calculator.add(firstNumber, secondNumber)
      const expectedResult = 15

      expect(addResult).to.equal(expectedResult)
    })

    it('should not add two non-number variables', () => {
      const firstNumber = 'test'
      const secondNumber = 4
      const addResult = calculator.add(firstNumber, secondNumber)
      const expectedResult = 'please provide numbers'

      expect(addResult).to.equal(expectedResult)
    })
  })

  describe('Subtract', () => {
    it('should subtract two number variables', () => {
      const firstNumber = 10
      const secondNumber = 3
      const subtractResult = calculator.subtract(firstNumber, secondNumber)
      const expectedResult = 7

      expect(subtractResult).to.equal(expectedResult)
    })

    it('should not subtract non-number variables', () => {
      const firstNumber = 'test'
      const secondNumber = 4
      const subtractResult = calculator.subtract(firstNumber, secondNumber)
      const expectedResult = 'please provide numbers'

      expect(subtractResult).to.equal(expectedResult)
    })
  })

  describe('Multiplication', () => {
    it('should multiply two number variables', () => {
      const firstNumber = 4
      const secondNumber = 3
      const multiplicationResult = calculator.multiplication(
        firstNumber,
        secondNumber
      )
      const expectedResult = 12

      expect(multiplicationResult).to.equal(expectedResult)
    })

    it('should not multiply non-number variables', () => {
      const firstNumber = 'test'
      const secondNumber = 4
      const multiplicationResult = calculator.multiplication(
        firstNumber,
        secondNumber
      )
      const expectedResult = 'please provide numbers'

      expect(multiplicationResult).to.equal(expectedResult)
    })
  })

  describe('Division', () => {
    it('should divide two number variables', () => {
      const firstNumber = 12
      const secondNumber = 3
      const divisionResult = calculator.division(firstNumber, secondNumber)
      const expectedResult = 4

      expect(divisionResult).to.equal(expectedResult)
    })

    it('should not multiply non-number variables', () => {
      const firstNumber = 'test'
      const secondNumber = 4
      const divisionResult = calculator.division(firstNumber, secondNumber)
      const expectedResult = 'please provide numbers'

      expect(divisionResult).to.equal(expectedResult)
    })

    it('should not divide by zero', () => {
      const firstNumber = 10
      const secondNumber = 0
      const divisionResult = calculator.division(firstNumber, secondNumber)
      const expectedResult = 'second number should not be 0'

      expect(divisionResult).to.equal(expectedResult)
    })
  })
})
