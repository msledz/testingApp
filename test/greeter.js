const expect = require('chai').expect
const Greeter = require('../app/greeter')

describe('Greeter', () => {
  describe('Creating new class instance', () => {
    it('should create new class instace without a provided name', () => {
      const greeter = new Greeter()
      const greetings = greeter.sayHello()
      const result = 'Hello Magda'

      expect(greetings).to.equal(result)
    })

    it('should create new class instance with provided name', () => {
      const greeter = new Greeter('Agnieszka')
      const greetings = greeter.sayHello()
      const result = 'Hello Agnieszka'

      expect(greetings).to.equal(result)
    })
  })

  describe('Set name', () => {
    let greeter

    beforeEach(() => {
      greeter = new Greeter()
    })

    it('should set default name when no name provided', () => {
      const defaultName = 'Magda'

      expect(greeter.name).to.equal(defaultName)
    })

    it('should set new name', () => {
      greeter.setName('Marcin')
      const newName = 'Marcin'

      expect(greeter.name).to.equal(newName)
    })

    it('should require name to be string', () => {
      const errorMessage = greeter.setName(4)
      const expectedMessage = 'name should be string'

      expect(errorMessage).to.equal(expectedMessage)
    })
  })
})
