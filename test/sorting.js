const expect = require('chai').expect
const sorting = require('../app/sorting')

describe('Sorting', () => {
  describe('Bubble sort', () => {
    it('should sort provided array', () => {
      const startArray = [7, 12, 1, 5, 90, 3]
      const sortedArray = [1, 3, 5, 7, 12, 90]
      const resultArray = sorting.bubbleSort(startArray)

      expect(resultArray).to.deep.equal(sortedArray)
    })
  })
})
